# vm-ubuntu,2,2,4,20,161,ubuntu
module "vm-ubuntu" {
    source      = "../modules/kvm"
    name        = "vm-ubuntu"
    count       = 2
    cpu_core    = 2
    memory_gb   = 4
    disk_gb     = 20
    vid         = 161
    os          = "ubuntu"
    notes       = "Create with terraform 2020-08-24"
}

# vm-centos,2,4,8,20,163,centos,centos for test
module "vm-centos" {
    source      = "../modules/kvm"
    name        = "vm-centos"
    count       = 2
    cpu_core    = 4
    memory_gb   = 8
    disk_gb     = 20
    vid         = 163
    os          = "centos"
    notes       = "centos for test"
}


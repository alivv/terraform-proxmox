
provider "proxmox" {
  version = "~> 0.1.0"
  pm_tls_insecure = true
  pm_api_url = "https://localhost:8006/api2/json"
  pm_user = "root@pam"
  # pm_password = "xxx"
  # use env var, export PM_PASS=password
}

variable "vid" {
  default = 180
}

resource "proxmox_vm_qemu" "cloudinit-test" {
    count = 2
    name = "terraform-vm-${count.index +1}"

    #notes
    desc = "Create with terraform"

    # PVE node name
    target_node = "n11"

    # The destination resource pool for the new VM
    # pool = "pool0"

    # The template name to clone this vm from
    clone = "ubuntu"
    full_clone = true

    # Activate QEMU agent for this VM
    agent = 1

    os_type = "cloud-init"
    cores = "2"
    # sockets = "1"
    # vcpus = "0"
    # cpu = "host"
    memory = "4096"
    # scsihw = "lsi"
    vmid   = "${count.index +var.vid}"

    # # # Setup the disk.
    disk {
        id = 0
        size = "30G"
        format = "qcow2"
        type = "scsi"
        # type = "virtio"
        storage = "data"
        # storage_type = "Directory"
        storage_type = "dir"
        iothread = true
        # ssd = true
        discard = "on"
        
    }

    # Setup the network interface and assign a vlan tag: 256
    # network {
    #     id = 1
    #     model = "virtio"
    #     bridge = "vmbr0"
    #     tag = 22
    # }

    # Setup the ip address using cloud-init.
    ipconfig0 = "ip=192.168.21.${count.index +var.vid}/24,gw=192.168.21.1"

    # sshkeys = <<EOF
    # ssh-rsa AAAAB3NzaC1....  @Elvin
    # EOF

    lifecycle {
        ignore_changes = ["bootdisk","cipassword","ciuser","nameserver","scsihw","sshkeys"]
    }

}


output "name" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.name}"

}
output "vmid" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.vmid}"

}
output "ip" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.ssh_host}"

}

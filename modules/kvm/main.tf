
resource "proxmox_vm_qemu" "cloudinit-test" {
    count = "${var.count}"
    name = "${var.name}-${count.index +1}"

    #notes
    desc = "${var.notes}"

    # PVE node name
    target_node = "n11"

    #template name
    clone = "${var.os}"
    full_clone = true

    #QEMU agent
    agent = 1

    os_type = "cloud-init"
    # cpu = "kvm64"
    cores = "${var.cpu_core}"
    memory = "${var.memory_gb * 1024}"
    vmid   = "${var.vid + count.index}"

    # # Setup the disk
    disk {
        id = 0
        size = "${var.disk_gb}G"
        format = "qcow2"
        type = "scsi"
        storage = "data"
        storage_type = "dir"
        iothread = true
        discard = "on"
    }

    # Setup the ip address using cloud-init.
    ipconfig0 = "ip=192.168.21.${count.index + var.vid}/24,gw=192.168.21.1"

    lifecycle {
        ignore_changes = ["bootdisk","cipassword","ciuser","nameserver","scsihw","sshkeys"]
    }
}


output "name" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.name}"
}
output "vid" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.vmid}"
}
output "ip" {
    value = "${proxmox_vm_qemu.cloudinit-test.*.ssh_host}"
}
